window.onload = function (){

// Image Array
var myImages = [
  {
    imgURL : 'img/Randoms/1.png',
    altText : "Vrisea (?) fenestralis. From: 'L'illustration horticole: Gand, Belgium: Imprimerie et lithographie de F. et E. Gyselnyck, 1854-1896. [https://biodiversitylibrary.org/page/15986306]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/2.png',
    altText : "Cancer serratus. From: 'Zoology of New Holland. Vol. 1. London: Printed by J. Davis: published by J. Sowerby, 1794. [https://biodiversitylibrary.org/page/40653621]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/3.png',
    altText : "Virginia quail, O. virginiana. From: 'Edinburgh journal of natural history and of the physical sciences. Edinburgh (etc.): Published for the proprietor (etc.), 1835-1840. [https://biodiversitylibrary.org/page/33665397]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/4.png',
    altText : "Odontoglossum lucianianum. From: 'L'illustration horicole: Gand, Belgium: Imprimerie et lithographie de F. et E. Gyselnyck, 1854-1896. [https://biodiversitylibrary.org/page/15010504]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/5.png',
    altText : "Tropidonotus sipedon. From: 'North American herpetology: Philadelphia, J. Dobson; 1842. [https://biodiversitylibrary.org/page/3991203]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/6.png',
    altText : "Thrinax barbadensis, Loddiges. From: 'L'illustration horicole: Gand, Belgium: Imprimerie et lithographie de F. et E. Gyselnyck, 1854-1896. [https://biodiversitylibrary.org/page/15986173]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/7.png',
    altText : "Hyla bicolor. From: 'Animalia nova sive species novae testudinum et ranarum: Monachii: F.S. Hbschmanni, 1824. [https://biodiversitylibrary.org/page/2948587]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/8.png',
    altText : "Gonyasoma viride. From: 'Descriptiones et icones amphiborium. Moachii, Stuttgartiae et Tubingae, Sumtibus. J.G. Cottae, 1828-1833. [https://biodiversitylibrary.org/page/43580107]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/9.png',
    altText : "Abudefduf leucopomus. From: 'The fishes of Samoa. Washington, Government print off, 1906. [https://biodiversitylibrary.org/page/32055010]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/10.png',
    altText : "Novacula caerulea. From: 'The natural history of Carolina, Florida, and the Bahama Islands: London: Printed at the expense of the author, and sold by W. Innys and R. Manby, at the West End of St. Paul's, by Mr. Hauksbee, at the Royal Society House, and by the author, at Mr. Bacon's in Hoxton, MDCCXXXI-MDCCXLIII (i.e. 1729-1747). [https://biodiversitylibrary.org/page/40680114]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/11.png',
    altText : "Elephant beetle, Scarabaeus elephas. From: 'Edinburgh journal of natural history and of the physical sciences. Edinburgh (etc.): Published for the proprietor (etc.), 1835-1840. [https://biodiversitylibrary.org/page/33665411]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  },
  {
    imgURL : 'img/Randoms/12.png',
    altText : "Hercules beetle, Scarabaeus hercules. From: 'Edinburgh journal of natural history and of the physical sciences. Edinburgh (etc.): Published for the proprietor (etc.), 1835-1840. [https://biodiversitylibrary.org/page/33665411]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
  }
]

// Random image puller for header
function headerBG(){

  var imgCount = 8; // Only like the first 8 for headers
  var num = Math.ceil( Math.random() * imgCount );
  document.getElementById('header').setAttribute("style", "background: url('img/Randoms/"+ num + ".png') no-repeat 0% 60% fixed #f7f7f7;");
  //document.getElementById('header-img').src=myImages[num].imgURL
  // baby's first javascript

  }

headerBG();

// Random footer image
function footerImg(){

  var imgCount = 12; // imgCount should equal the number of items in the image array (but don't like #10 for footer)
  var num = Math.ceil( Math.random() * imgCount ) - 1; // Chooses a random number from 1-6; then subtracts 1 (because in Javascript array items start with 0)
  //document.getElementById('footer-img').src="img/Randoms/"+num+".png"; // old randomization script - just pulled an image from the folder; no array involved.
  document.getElementById('footer-img').src=myImages[num].imgURL
  document.getElementById('footer-img').alt=myImages[num].altText
  document.getElementById('footer-img').title=myImages[num].altText

}

footerImg();

};
