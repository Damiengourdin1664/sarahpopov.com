---
title: "Consulting"
date: 2021-09-21T15:18:51-04:00
draft: true
weight: 2
menu: "main"
---

## Ecological Data Management

>My educational background is in sustainability and biological sciences, and my work focuses on bridging the gap between raw data and actually being able to tell a story with that data.

To make an impact with your hard-earned data, you need two key ingredients: **first**, a good platform to stand on, where your data are all clean and organized; and **second**, a beautiful way of telling your data story. I can do both. To see some of my personal favorites out of my work portfolio, check out my [featured projects](/projects). I've also listed some of my other skills and smaller projects below.

I'm available for a wide variety of analytical and data-management consulting projects. My specialties lie in the technical details: if you need someone to analyze and map data, build a beautiful, eye-catching website or web application to explore your data, or custom-build a database to house all your data, I can do all of that for you.

My **current project** focuses on database design for {{< font-color orange >}}salmonid genomics data{{< /font-color >}} for the Government of Canada's {{< font-color red >}}Department of Fisheries and Oceans{{< /font-color >}}.

## Clients

{{< logo-gallery "Clients" >}}

### Data Management

Have a giant dataset that needs to be wrangled? I specialize in relational database management, and can get your data under control - using whatever platform suits you best, be it MS Access, SQLite, MySQL, or PostgreSQL. Check out [SOMNI db](/projects/somni-db), the database project I'm most proud of.

When it comes to biological data specifically, I am well aware as any that it's rarely neat out of the box. Mishaps in the field happen, improvements or changes in your note-taking protocol can occur through time, that one grad student can leave the lab and all data documentation disappears with them. In my years as a data analyst for several lab groups I've mastered a suite of tools, such as {{% font-color blue %}}**Open Refine**{{% /font-color %}} and **R**, to quickly and efficiently clean those ugly duckling datasets, and can do the same for you.


### Map-making

I have experience in doing spatial analysis and making publication quality maps with a wide array of GIS platforms, including: ArcGIS products, QGIS, R, and PostGIS. I have over 5 years of experience in spatial database administration, primarily with PostgreSQL/PostGIS on Amazon AWS.

{{< figure src="/img/Consulting/ratchet-PPR-animation.gif" caption="One of the main aims of the [*Sea Around Us*](http://www.seaaroundus.org) project, based out of the University of British Columbia, is to highlight the exploitation of the world's marine resources with eye-catching graphics. In this particular project I was asked to create an animation using several million rows of data to show the ever growing influence of fisheries on our oceans." >}}


{{< figure src="/img/Consulting/sarah-gergel-map.png" caption="For [sarahgergel.net](http://sarahgergel.net/lel/research/), the client wanted an interactive map that could easily be updated on the back end. I created a simple R Shiny application that automatically displays any new projects added to the projects csv file." >}}


### Web Design

While in university I made some extra money with a web design side hustle. I designed <a>sarahpopov.com</a> website by hand, the old fashioned way, with `html`, `css`, and a dash of `javascript`, as a personal project and portfolio piece. It's all managed with {{% font-color dark-chartreuse %}}**Git**{{% /font-color %}} and powered by {{% font-color purple %}}**Hugo**{{% /font-color %}}. Read more about it [here](/colophon).

{{< figure src="/img/Consulting/gotham-sailing.png" caption="One of my university projects was to design and build the website for the now-defunct Gotham Sailing. Gotham Sailing was a private sailboat charter service that allowed guests to cruise around Manhattan." >}}