---
title: "About"
date: 2021-09-28T09:48:20-04:00
draft: false
portrait: 'img/srha icon.png'
---

#### Sarah Popov, *MSc*

I'm a marine ecologist with a strong data science background. Always looking for opportunities to grow as a scientist. Currently available for consulting. <!--more-->

### Clients

{{< logo-gallery "Clients" >}}

### Research

My current research interests lie in:
1. Theoretical ecology in the marine environment, with particular focus on dietary niche and community dynamics;
2. The use of eDNA and stable isotopes to explore marine food webs;
3. The importance of large storm events in shaping marine communities.

I also love helping other lab groups get a handle on monstrous amounts of data and working with databases. If you have any active research or data management opportunities you think I'd be a good fit for, please reach out to inquire about consulting.

##### Get in touch

{{% font-color blue %}}sarah [dot] popov [at] mail.mcgill.ca{{% /font-color %}}

##### Follow me

The best way to get in touch is to email me - I've drastically cut back on my social media consumption in the last year. But in case you still wish to follow my occasional updates..

{{< partial "socials.html" >}}
