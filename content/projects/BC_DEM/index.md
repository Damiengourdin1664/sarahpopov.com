---
title: "A quick and easy way to download BC digital elevation model data"
subtitle: "The BC_DEM() function"
date: 2023-05-28T22:03:37-07:00
draft: false
categories: ['featured']
tags: ['gis', 'R']
summary: "A quick and easy way to download British Columbia digital elevation model (DEM) data, gridded to a 25 m resolution."
featured_image: "SRTM.png"
featured_alt: "Digital elevation data for the Vancouver area."
featured: true
---

{{< post-image src="SRTM.png" caption="Example image of digital elevation data that has been reposted on the internet so much that quite frankly I have no idea what it is or who to credit the image to." alt="Example image of digital elevation data that has been reposted on the internet so much that quite frankly I have no idea what it is who to credit the image to." width="300px" />}}

So, you're a GIS professional or graduate student or government scientist working in beautiful British Columbia. The air is pure, the waters are clear, and the mountains carefully photographed to a 1-2m precision by a ceaselessly orbiting battalion of satellites in space to produce free, publicly available digital elevation model (DEM) data. What a time to be alive.

You'd think being able to download the [freely available BC DEM data](https://www2.gov.bc.ca/gov/content/data/geographic-data-services/topographic-data/elevation/digital-elevation-model) from the BC provincial website would be easy, right? Where you can just pick a map tile to download and easily download a raster file? **Wrong.** The link to download the 25 m resolution DEM tiles sends you to a FTP server page with dozens of inexplicably named folders to sift through:

{{< post-image src="public_dem_data.png" caption="Hey kid, wanna download some free 25 m resolution DEM data? Just click through these several dozen links and trust us..." alt="Hey kid, wanna download some free 25 m resolution DEM data? Just click through these several dozen links and trust us..." />}}

Eventually, after clicking through the provincial website a bit more and inspecting the $500 DEM dataset, it became clear that these folder names correspond to the [BC Maps & Orthos online store](https://a100.gov.bc.ca/ext/mtec/public/products/mapsheet) download tiles. As far as I can tell there's no real rhyme or reason behind the letters or numbers, but the letters and numbers at least match up with all the directories listed on the FTP server.

{{< post-image src="BC-map-tiles.png" caption="BC Maps & Orthos online store map tiles." alt="BC Maps & Orthos online store map tiles." />}}

So, now you know what links to actually click on to download a DEM! So you click "92g" with enthusiasm, and you think that you'll shortly be on your way to easily and efficiently plot up the mountains on the north shore of Vancouver? **Wrong.** The link brings you to another several dozen links of individually zipped DEM files.

{{< post-image src="zipped-dem-strips.png" caption="Zipped DEM strips that make up a single map tile of data." alt="Zipped DEM strips that make up a single map tile of data." />}}

So, to download a single map tile of data, you need to *first* download every single zip file, unzip them all, then merge them in your GIS program of choice. Good luck manually doing this if you want to, say, download the free DEM data for all of Vancouver Island.

{{< post-image src="92b-strips.png" caption="First four files in the 92b folder, unzipped and plotted in QGIS. This is, in fact, supposed to be Victoria. Only 15 more files to download, unzip, and plot to go!" alt="First four files in the 92b folder, unzipped and plotted in QGIS. This is, in fact, supposed to be Victoria. Only 15 more files to download, unzip, and plot left to go!" />}}

For a project I'm currently working on, I really needed 900m elevation data **for the entire province!** No way was I going to spend a week trying to sort through all these download links and files. A colleague had warned me that this whole thing (i.e., "can I have a single polygon of 900m elevation data") would be quite labor intensive. I had naively thought this data would be easy to get ahold of, and instead was confronted with this FTP maze!

**Solution:** make R do all the work for you. As part of my work with Environment & Climate Change Canada, I've been developing an R package called [`{MAMU}`](https://github.com/popovs/MAMU) (short for MArbled MUrrelet) to bundle up my workflows and analyses. It's still in development, but the map tile download functions are a fully operational battlestation. Simply download `{MAMU}`, run a one-liner of code, and voilà, your full-sized raster is ready to go for whatever future analyses you need. Code below and also [here in this little vignette](https://popovs.github.io/MAMU/articles/BC_DEM.html).

```r
# install.packages("devtools")
devtools::install_github("popovs/MAMU")

library(MAMU)

# Download Vancouver-area DEM
dem <- BC_DEM("92g",
              save_output = FALSE)
#> Stitching together your DEM...
#>
|---------|---------|---------|---------|
=========================================


terra::plot(dem)
```

{{< post-image src="92g.png" caption="Digital elevation data for the Vancouver area." alt="Digital elevation data for the Vancouver area." />}}