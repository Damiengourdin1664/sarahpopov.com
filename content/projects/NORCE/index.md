---
title: "PATHdna"
subtitle: "Another salmonid disease water monitoring project"
date: 2022-10-13T19:54:10-04:00
draft: true
categories:
tags: ['NORCE', 'SQL', 'SQLite', 'salmon', 'eDNA']
summary: "In this collaboration between Norwegian and Canadian researchers, I'm providing a custom-built `R` + `SQLite` data management pipeline for reams of eDNA data."
featured_image: 'norce-logo.png'
featured_alt: "Logo for the Norwegian Research Centre (NORCE)."
featured: false
---

After my contract at DFO wrapped up, my former boss connected me with her frequent collaborators in Norway - a research group based out of the Norwegian Research Centre (NORCE). They were starting up a project called "PATHdna", whose main aim was to develop a rapid water monitoring system to test for waterborne salmonid diseases around salmon farms. As I had recently developed the data pipeline [for a similar project with DFO](link to namgis project here), my former boss suggested that I might be a good fit for PATHdna.

More info/text here eventually?