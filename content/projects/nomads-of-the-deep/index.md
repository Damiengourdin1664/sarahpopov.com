---
title: "Nomads of the Deep"
subtitle: "The Untold Story of the Arctic Abyss"
date: 2021-09-24T12:16:02-04:00
draft: false
##weight: 2
categories: ['Featured']
tags: ['writing', 'maps']
summary: "Beautiful and compelling science communication on the ArcGIS StoryMaps platform."
featured_image: 'halibut.png'
featured_alt: "149 Halibut. From: 'Our country's fishes and how to know them: a guide to all the fishes of Great Britain. [https://www.biodiversitylibrary.org/page/20965821#page/56/]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
---

Science regularly produces results, but it's not always easy to translate those results to a non-scientific audience. In 2018 I was hired by the [Hussey Lab](https://www.husseylab.com) in partnership with [WWF Canada](https://wwf.ca) to create a beautiful and compelling story that showcased the lab's recent important work on Greenland halibut migration.

<iframe src="https://storymaps.arcgis.com/stories/53afd0a9dcdf4f31895ffc7805fae352" width="100%" height="500px" frameborder="0" allowfullscreen allow="geolocation"></iframe>

The clients asked for a way to use mapmaking to highlight the lab's animal movement research, which ultimately resulted in a major policy change and conservation win in the Canadian Arctic. I suggested that **ArcGIS StoryMaps** may be a compelling way to share the research, as the research itself has a heavy spatial component, and the clients agreed. The result was *The Nomads of the Deep: The Untold Story of the Arctic Abyss*, published on the **ArcGIS StoryMaps** platform. I wrote the text and created the content within the story.

My favorite part? WWF Canada reached out to a translation service in Nunavut and had the StoryMap translated into Inuktitut. ᑐᓐᖓᓱᒋᑦ ᐸᓐᓂᖅᑑᑉᐃᑭᖓᓄᑦ!

The above StoryMap has been updated since publication in order to fit the latest available template from ArcGIS. The original StoryMap as it was published in 2018 is still available [here](https://www.arcgis.com/apps/Cascade/index.html?appid=2d126704500a47668ed5e83741695112).

{{< post-image src="halibut.png" caption="Halibut, the main focal species of this research. From: ['Our country's fishes and how to know them: a guide to all the fishes of Great Britain'](https://www.biodiversitylibrary.org/page/20965821#page/56/)" />}}