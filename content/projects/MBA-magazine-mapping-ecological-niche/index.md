---
title: "From the Mountaintops to the Deep Sea: Mapping Ecological Niche"
subtitle: "Scientific writing & Illustration"
date: 2021-09-24T12:16:48-04:00
draft: false
##weight: 1
categories: ['featured']
tags: ['writing', 'illustration']
summary: "A sharky article I wrote about my MSc research for The Marine Biologist magazine."
featured_image: 'Whiskery_shark_hunt.png'
featured_alt: 'Illustration of a whiskery shark hunting its cephalod prey which I illustrated for the article.'
featured: true
---

{{< post-image src="Whiskery_shark_hunt.png" caption="A whiskery shark hunts for octopus in the coral reef. This was one of the illustrations I made to accompany the article." />}}

In 2020, I was contacted by Guy Baker, editor of [The Marine Biologist](https://www.mba.ac.uk/marine-biologist) magazine, who asked me if I would be interested in writing an article about my MSc research for the magazine. I had given a talk at the Marine Biological Association (where the magazine is based out of) a few months earlier and it was received well, so Guy thought an article might be a good fit.

A personal goal of mine is to ensure all the science I produce is accessible - not confined to the ivory tower of academia - so I was excited at the opportunity. I'd also been playing around with a way to visually tell the story of my MSc thesis, which explores the relationship between shark and ray diets and depth. The result: a cover illustration and article for the MBA magazine, titled *From the Mountaintops to the Deep Sea: Mapping Ecological Niche*.

{{< post-image src="cover-mockup.jpeg" caption="Cover mockup of the issue." width="550px" />}}

{{< post-image src="initial-cover.jpeg" caption="Initial cover idea. I really liked the mountains in the sharks and the floating clouds in the stingrays, but unfortunately it meant that text on top wasn't very readable." width="550px" />}}

#### A lengthy process

This was my first time illustrating something for a cover, where lots of text needed to be superimposed on top. It took a few back-and-forths with the editor to go from "nice illustration" to "workable cover".

{{< album src="process" >}}