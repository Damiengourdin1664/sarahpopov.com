---
title: "SOMNI db"
subtitle: "A custom-built telemetry database"
date: 2021-09-21T15:18:51-04:00
draft: true
#weight: 4
categories: ['Projects']
tags: ['SQL', 'databases', 'telemetry']
summary: "House your animal movement data in a powerful PostgreSQL database with full PostGIS integration."
featured_image:
featured_alt:
---

>SOMNI db is an open-source database built on PostgreSQL so that research groups can house and manage their valuable acoustic telemetry data. Full PostGIS support means querying your data by location is an absolute breeze.

Prior to starting my MSc degree, I was brought on by the [Hussey Lab](https://www.husseylab.com) to help organize and manage their massive acoustic telemetry datasets. The result is <a>SOMNI db</a>, a custom database built on **PostgreSQL** and hosted on a **DigitalOcean** server. One animal that the lab regularly studies is the Greenland shark, *Somniosus microcephalus*. Drawing inspiration from this, I decided to name the database after the shark.

I am still working on packaging the database in the most user-friendly way possible, but if you are interested in seeing the database structure yourself, just send me an email at sarah [dot] popov [at] mail [dot] mcgill [dot] ca. I'll be more than happy to send you a Postgres DDL.

<!-- One of the Hussey Lab's primary research activities is the tagging and tracking fish. Fish are caught and then surgically implanted with a small tracking device, which pings out a unique ID code every few seconds. This unique ID is then picked up by underwater listening stations that the lab has deployed around the Canadian Arctic. Researchers then go out and download these data from these listening stations. But what to do with all that data afterwards is a bit more difficult. That's where SOMNI db comes in:
-->
