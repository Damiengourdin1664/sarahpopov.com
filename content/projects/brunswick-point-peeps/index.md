---
title: "Brunswick Point Peeps"
subtitle: "Analyzing shorebird marsh usage in a heavily industrial area"
date: 2022-10-29T11:28:37-04:00
draft: true
categories:
tags: ['seabirds', 'ECCC']
summary: "Brunswick Point is in Delta, BC, right next to one of the busiest ports on the west coast of North America. It also happens to be one of the largest marshes used by migrating shorebirds in the world. How does this heavily modified marsh impact peep spatial distributions?"
featured_image:
featured_alt:
featured: false
---