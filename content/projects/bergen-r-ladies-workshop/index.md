---
title: "Bergen R-Ladies Workshop"
subtitle: Making and Publishing Your First R Package
date: 2022-08-17T20:58:59-04:00
draft: false
categories: ['featured']
tags: ['R', 'tutorial']
summary: "In August I had the absolute pleasure of not only visiting Bergen, but also giving a workshop to the local R-Ladies group on how to make a simple R package, `fjoRds`, push it to Github, and create a `{pkgdown}` website to accompany it."
featured_image: 'fjords_hex.png'
featured_alt: "R hexagon patch for the 'fjoRds' package developed in this tutorial."
featured: true
---

In August I had the absolute pleasure of not only visiting Bergen, but also giving a workshop to the local R-Ladies group on how to make a simple R package with `{usethis}`. Together, we live-coded a package from scratch called `fjoRds`, [pushed it to Github](https://github.com/popovs/fjoRds), and made a `{pkgdown}` [website to accompany it](https://popovs.github.io/fjoRds/). The slides and also full presentation (it was recorded!) are below.

{{< post-image src="fjords_hex.png" caption="R hexagon patch for the `fjoRds` package developed in this tutorial. This simple package allows you to check if a given fjord is in Norway or not." width="278px" />}}

### The slides

Click on the slides below to navigate with your arrow keys. Or, if you prefer, check out the [slides full screen in a separate window](https://popovs.github.io/r-pkg-slides/).

<iframe src="https://popovs.github.io/r-pkg-slides/" frameborder="0" allowfullscreen></iframe>

The code that makes these slides is [on Github here](https://github.com/popovs/r-pkg-slides), if you want to see how they were made.

#### In summary

- We create a new `R` project that will house our package
- We write a (very simple) function that checks if a fjord is in Norway:
```
fjord_finder("Sognefjord")
# "Sognefjord is a nice Norwegian fjord!"

fjord_finder("Sechelt Inlet")
# "Sechelt Inlet - I see you're in Canada, eh!"

fjord_finder("Illulissat")
# "Illulissat? Never heard of em."
```
- We learn how to add references to outside (imported) packages and how to bundle a dataset with our package
- And finally, we push the project to Github and create a `{pkgdown}` website for our package!

### The presentation

{{< youtube id="9ZKGLv84nZY" title="R-Ladies Bergen (English) - Making and publishing your first R package - Sarah Popov" >}}

You'll have to excuse the few technical hiccups - I only had one screen to work with so the screensharing wasn't optimal.

### Resources

I can't take full credit for this presentation - this tutorial is modified from a phenomenal tutorial I originally found on [Matt Dray's Rostrum blog](https://www.rostrum.blog/2019/11/01/usethis/) (which helped *me* make *my own* first package!). However, the code in that tutorial was already a bit outdated by this point, so I updated it with the latest function usage.

In addition to Matt Dray's resource suggestions, I would also add Patricio R Estévez-Soto's post, [Your first R package: A tutorial](https://www.prestevez.com/post/r-package-tutorial/), to the list.
