---
title: "Freelance data scientist"
subtitle: "With a niche in ecology"
date: 2022-10-25T12:46:25-04:00
draft: false
featured_image: "img/10.png"
featured_alt: "Squalus glaucus. From: 'D. Marcus Elieser Bloch's, ausübenden Arztes zu Berlin. ... Oeconomische Naturgeschichte der Fische Deutschlands. ... Berlin: Auf Kosten des Verfassers und in Commission bei dem Buchhändler Hr. Hesse, 1782-1795. [https://biodiversitylibrary.org/page/48057840]' Image modified from the Biodiversity Heritage Library, an amazing free initiative seeking to digitize and share biodiversity knowledge."
---

We all know we could be a bit better with our data. I'm here to help sort that out.

[//]: # (If you need someone who can wrangle big fish and big data in one go (and I mean both quite literally!), let's have a chat. I'm available for an array of consulting work, ranging from fieldwork to scientific software development. Check out my [consulting](/consulting) page to see a full range of what I can do.)

[//]: # (Messy fieldbook data still not typed up? (Be honest, we all know there's at least one that's not typed up.) Or maybe you're ahead of the curve, and you know it's been entered into `Field_DATA_2012final_2.xlsx`. Since it's been kicking around for 10 years you figure it's time to analyze. Until you open the file up and realize there's 6 tabs and no way `R` or `Python` will ever work with this.)
[//]: # (I've been there. What ecologist hasn't, really? We all know we could be a bit better with our data. I'm here to help sort that out.)
[//]: # (I'm an ecologist and data scientist for hire: available for intensive data cleaning, database building, and analysis.)