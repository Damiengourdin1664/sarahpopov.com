---
title: "Colophon"
date: 2021-09-29T09:32:41-04:00
draft: false
---

#### In summary

I hard-coded this website in `HTML` and `CSS` in the {{% font-color yellow %}}**Atom**{{% /font-color %}} text editor, using [HTML5 Boilerplate](https://html5boilerplate.com) and [Skeletonize CSS](http://getskeleton.com) as my backbone. I modified imagery from the [Biodiversity Heritage Library](https://www.biodiversitylibrary.org) and created a few simple `Javascript` scripts to display random images on my website. The nice fonts come from [Google Fonts](https://fonts.google.com). My content is all written in **Markdown**, and I use [Hugo](https://gohugo.io) to bundle it all together. It's hosted using [Netlify](https://www.netlify.com). Check out my [GitLab repo](https://gitlab.com/popovs/sarahpopov.com) to see the guts of this website.

##### In a bit more detail

>Y'all remember Neopets?

Yeah. That's where this all began. *Neopets.* If you wanted to have a cool petpage on Neopets dot com, you needed to get good at HTML and CSS. So that's just what I did. Javascript was banned so it took me quite a few years later to pick it up.

I've had a draft <a>sarahpopov.com</a> kicking around for years, ever since I registered the domain in high school in ~2011. It wasn't until 10 years later that I actually sat down and finally.. did the thing. By this point my needs had grown from displaying a simple single-page CV. I needed pages! Content! A CMS! Thankfully in the 10 years I've been procrastinating this website, the {{% font-color purple %}}**Hugo**{{% /font-color %}} static site generator (SSG) was invented and perfected. [Mike Dane's Hugo tutorial series](https://www.youtube.com/watch?v=qtIqKaDlqXo) was indispensable for me to get this project off the ground.

The vintage natural history imagery on this website all comes from the incredible [Biodiversity Heritage Library Flickr page](https://www.flickr.com/photos/61021753@N02/). These folks have spent countless hours digitizing natural history content and uploading high resolution imagery to the web *free for anyone to use*. Just amazing. I spent a few days going through their files and picking out pictures I liked, and tried to do my best to pick things *other* than just fish. I then cleaned them up in iPad Procreate for my website.

Why {{% font-color orange %}}**GitLab**{{% /font-color %}} over **GitHub**? Well, {{% font-color orange %}}**GitLab**{{% /font-color %}} is open-source. When GitHub was acquired by Microsoft in 2018 I migrated my stuff over. I don't know if it was meaningful at all to migrate over, but once I did, it felt silly to migrate all my stuff back. So here we are.

{{% font-color blue %}}**Netlify**{{% /font-color %}} has been amazing to work with. Really, my only issue with it is it's really easy to type it out incorrectly as Neti**fly** :butterfly: in the config files. Whenever I make a change on my local machine and then push it to GitLab, my website automatically updates, thanks to Netlify. [Here's a really good tutorial](https://dolugen.com/how-to-publish-your-website-with-hugo-and-netlify/) on the basics of hosting a Hugo website with Git and Netlify.