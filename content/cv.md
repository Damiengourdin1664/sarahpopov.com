---
title: "CV"
date: 2021-09-21T15:18:51-04:00
draft: false
weight: 4
menu: "main"
---

## Profile

Data scientist with experience in marine ecology and spatial analysis. Enjoys working with big data with conservation applications. Looking to grow as a scientist.

---

## Skills

### Languages

English, Russian, `R`, `SQL`, `HTML`, `CSS`

### Technical

- GIS (ArcGIS, QGIS, `R`, PostGIS)
- Large relational database administration (Microsoft Access, `PostgreSQL`, `MySQL`, `SQLite`)
- Descriptive statistics in `R`
- Mixed effects modeling in `R`
- `R Shiny` web applications
- Git (@popovs)
- Scientific figure creation and scientific illustration
- MS Office Suite

### Professional

- Scientific and technical writing, incl. grant writing
- Public speaking

### Other

- BC Drivers License
- Small non-pleasure domestic vessel safety (SDV-BS) certified
- PADI dive certified
- Vemco/Innovasea system maintenance (receiver turnover, tagging fish)
- Living and working aboard research vessels at sea for extended periods of time

---

## Experience

### Freelance Data Analyst
#### Popov EcoData
##### July 2021—Present
- Founded Popov EcoData to fill the gap between biologists trained in natural sciences and data analysts trained in computer sciences. I excel at both.
- Current projects:
  - Forecasting marbled murrelet population density around the province of British Columbia. Currently developing the `R` package [`{MAMU}`](https://github.com/popovs/MAMU) to streamline analyses. Client in Environment and Climate Change Canada (ECCC).
  - Developing a data reporting system for a salmonid eDNA project called “PATHDNA”. Client is the Norwegian Research Centre (NORCE).
  - Preparing and analyzing longterm shorebird monitoring data of peeps visiting marshes around Delta, BC. Client is Environment and Climate Change Canada (ECCC).
- Developed a [high speed data reporting system](projects/rnamgis) for salmonid disease prevalence at various salmon aquaculture sites (built on `R` + `SQLite`). Client was the Pacific Salmon Foundation in collaboration with the 'Nam̱gis First Nation.

### Project partner
#### Norwegian Research Centre (NORCE)
##### June 2022-Present
- Data scientist and project collaborator on the NORCE PATHdna project, an eDNA water monitoring project investigating disease prevalence around salmon farms in western Norway.
- Designing and developing the data reporting and storage system for the project.

### Wildlife Biologist (BI-03)
#### Environment and Climate Change Canada, Delta, BC
##### Dec 2021-April 2022

- Biologist for the Canadian Federal government working
under the [Oceans Protections Plan](https://tc.canada.ca/en/initiatives/oceans-protection-plan). Key deliverables included calculating and mapping areas of high biological importance for various species of west coast sea birds, including tufted puffins, Cassin’s auklets, and rhinoceros auklets, to provide to emergency responders in case of marine oil spills.
- Co-authored a custom-made `R` package, [`OPPTools`](https://github.com/popovs/OPPtools), to analyze data. Data consisted of satellite-tagged (GPS & GLS) sea bird locations.
- Conducted banding fieldwork of glaucous-winged gulls.

### Aquatic Science Technician (EG-05)
#### DFO Pacific Biological Station, Nanaimo, BC
##### Jan 2021-June 2021

- Data analyst for the DFO Molecular Genetics Laboratory (PI: Dr. Kristi Miller). Key responsibilities included managing large genomics Access database, preparing new data files for import, preparing data packages for export, and creating maps and figures.
- Liaised with various outside collaborators, e.g., the Strategic Salmon Health Initiative, University of Alaska, Gunther Analytics, and Strait of Georgia Data Centre to ensure smooth data pipeline.
- Created and maintained all new data pipeline scripts, new Git repository to house code and keep track of data issues, and data wiki in order to ensure document data pipeline for future data administrators.
- Assisted in labwork, e.g. eDNA collection and smolt dissections.

### Research Assistant, Information Analyst, & Database Administrator
#### Hussey Lab, University of Windsor
##### May 2018-Aug 2020

- Designed, created, and managed the Hussey Lab’s (PI: Dr. Nigel Hussey) acoustic telemetry database, SOMNI db (PostgreSQL; >10 million rows).
- Extracted data and prepared datasets for other lab members and outside collaborators to meet their research needs. Collaborated with Ocean Tracking Network to facilitate data transfers between Hussey Lab and OTN.
- Created StoryMaps in partnership with WWF-Canada to facilitate outreach with stakeholders in the north. Work was later translated to Inuktitut.
- Fieldwork aboard longlining vessel Kiviuq in the remote Davis Strait. Tagged, measured, and sampled porcupine crab (Neolithodes grimaldii) & Greenland halibut (Reinhardtius hippoglossoides). Serviced deepwater telemetry array composed of EdgeTech releases and Innovasea (Vemco) acoustic receivers.

### Research Assistant & Information Analyst
#### *Sea Around Us*, University of British Columbia
##### Jan 2016-Apr 2018

- Carried out GIS needs for Sea Around Us (PI: Dr. Daniel Pauly) in order to map catch, primary production, and fishing effort by EEZ, FAO region, NAFO division, etc. at a global scale.
- Managed the Sea Around Us spatial database of over 1.7 million rows (PostgreSQL).
- Reconstructed country-by-country historical fisheries catch with both reported and unreported landings for countries throughout the world and published results in grey and peer-reviewed literature, incl. the Fisheries Center Research Reports, Frontiers in Marine Science and Fisheries Research.
- Assisted in workshops with Sea Around Us partners both at UBC and abroad.

---

## Education

### MSc, Biological Sciences
#### University of Windsor | 2018-2020
##### Thesis: *Breadth of the Wild: Global patterns in elasmobranch dietary niche breadth*
##### Supervisor: Dr. Nigel Hussey

Investigated global scale marine predator biogeography and discovered a novel depth-niche gradient in elasmobranchs (sharks, skates, rays). Work involved categorizing hundreds of elasmobranchs by biogeographic region, functional group, and conservation status. Calculated dietary, isotopic, and spatial niche breadth using large diet, tissue, and satellite telemetry databases (R, SQL, Excel).

### BA&Sc, Sustainability, Science, & Society
#### McGill University | 2012-2015
##### *First Class Honours, with distinction*
##### *Dean's Multidisciplinary Undergraduate Research List*
###### Thesis: *A Tale of Two Highways: Two Fishes' Residency on Ocean Tracking Network's Halifax Receiver Line*
###### Supervisor: Dr. Frederick Whoriskey

Determined fish movement and residency patterns around Ocean Tracking Network’s Halifax detection line with a novel fish residency index. The research revealed a group of juvenile female blue sharks (Prionace glauca) living in the waters off Halifax and confirmed that the southern Scotian shelf was a major migration pathway of threatened North American Atlantic salmon (Salmo salar).

---

## Conferences & Presentations

### 2021 Ocean Tracking Network Symposium
#### Halifax, NS, CA | Nov 16 2021

**Popov S.**, Davidson E., Orrell D. *Sit Tight or Flight: How Marine Organisms Weather the Storm.* (Oral Presentation - International Conference)

### British Ecological Society
#### Belfast, NI | Dec 14 2019

**Popov S.**, Barkley A., Lees K. J., Hussey, N. E. *Latitudinal patterns of elasmobranch dietary niche breadth.* (Oral Presentation - International Conference)

### Marine Biological Association
#### Plymouth, UK | Dec 05 2019

**Popov S.** *Latitudinal patterns of elasmobranch niche breadth.* (Oral Presentation - Invited Guest Lecture) Host: Dr. David Sims, Marine Biological Association

### Canadian Society of Zoology
#### Windsor, ON, CA | May 13-17, 2019

**Popov S.**, Barkley A., Hussey, N. E. *Breadth of the Wild: global patterns of dietary niche across latitude in the marine realm.* (Oral Presentation - National Conference)

---

## Workshops

#### Bergen R-Lades: Making and Publishing Your First R Package
##### August 2022

*Presenter.* Led a workshop for a dozen members of the Bergen R-Ladies group (Bergen, Norway) on how to make a simple R package, push it to Git, and create a working website complete with documentation and examples for your new package. The full workshop was recorded and [available online](/projects/bergen-r-ladies-workshop/).

#### University of British Columbia
##### Autumn 2017

*Attendee.* SciCATS: Open access, modular science communication training workshop.

#### Saly, Senegal
##### February 2017

*Attendee and presenter.* Represented Sea Around Us as a stakeholder in two West African fisheries workshops and assisted primary workshop facilitator Dr. Dyhia Belhabib (Ecotrust Canada). My purpose at the workshops was to help formulate research questions that needed to be answered in the region and describe the role Sea Around Us could play in helping answer these questions in the future.

1. Surexploitation des petits pélagiques / pêche industrielle [Over-exploitation of small pelagics / industrial fishing]. Feb 20-22, 2017. Saly, Senegal. *In French.*

2. Gestion des sites côtiers importants pour les petits pélagiques [Management of critical coastal marine habitats for small pelagics]. Feb 23-24, 2017. Saly, Senegal. *In French.*

#### University of British Columbia
##### Autumn 2016

*Attendee.* Biodiversity Research: Integrative Training & Education (BRITE) Workshop: Spatial Data and Analysis in Ecological Systems

---

## Research Grants & Awards

|                                                   |            |             |
|---------------------------------------------------|------------|-------------|
| NSERC CGS Masters                                 | $17500 CAD | 2019-2020   |
| Ontario Graduate Scholarship (MSc - declined)     | $15000 CAD | 2019-2020   |
| MITACS Globalink Research Award                   | $6000 CAD  | Fall 2019   |
| Northern Scientific Training Program (NSTP) Award | $2400 CAD  | Spring 2019 |
| A.R. and E.G. Ferris Endowment Award              | $1000 CAD  | Spring 2019 |
| University of Windsor Entrance Scholarship        | $7500 CAD  | 2018-2020   |
| Science Undergraduate Research Award (SURA)       | $5600 CAD  | Summer 2014 |
