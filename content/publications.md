---
title: "Publications"
date: 2021-09-21T15:18:51-04:00
draft: false
weight: 3
menu: "main"
---

These are my selected publications only. To see my full publications list, please visit my [Google Scholar](https://scholar.google.com/citations?user=6J1bgFAAAAAJ&hl=en) profile.


### Peer reviewed contributions

{{< pub >}}

Bastien G, Barkley A, Chappus J, Heath V, {{% font-color magenta %}}**Popov S**{{% /font-color %}}, Smith R, Tran T, Currier S, Okpara P, Owen V, Madigan D, Franks B, Hueter R, Fischer C, McBride B, and Hussey N. **2020**. *Inconspicuous, recovering, or northward shift: Status and management of the white shark (Carcharodon carcharias) in Atlantic Canada.* Canadian Journal of Fisheries and Aquatic Sciences. {{< doi "10.1139/cjfas-2020-0055" >}}

Coulter A, Cashion T, Cisneros-Montemayor AM, {{% font-color magenta %}}**Popov S**{{% /font-color %}}, Tsui G, Le Manach F, Schiller L, Palomares MLD, Zeller D, and Pauly D. **2019**. *Using harmonized historical catch data to infer the expansion of global tuna fisheries.* Fisheries Research. {{< doi "10.1016/j.fishres.2019.105379" >}}

{{% font-color magenta %}}**Popov S**{{% /font-color %}} and Zeller D. **2018**. *Reconstructed fisheries catches in the Barents Sea: 1950-2015.* Frontiers in Marine Science. {{< doi "10.3389/fmars.2018.00266" >}}

{{< /pub >}}


### Data contributions

{{< pub >}}

Feyrer JF, Trueman C, Plint T, Hersh T, {{% font-color magenta %}}**Popov S**{{% /font-color %}}, Sabin RC. **2023**. *Stable isotopes of northern bottlenose whales in the eastern North Atlantic.* OSF. {{< doi "10.17605/OSF.IO/ASXNR" >}}

{{< /pub >}}


### Conferences & Presentations

{{< pub >}}

{{% font-color magenta %}}**Popov S**{{% /font-color %}}, Davidson E, Orrell D. **Nov 16, 2021**. *Sit Tight or Flight: How Marine Organisms Weather the Storm* [Oral Presentation - International Conference]. Ocean Tracking Network Symposium, Halifax, NS, CA.

{{% font-color magenta %}}**Popov S**{{% /font-color %}}, Barkley A, Lees KJ, Hussey, NE. **Dec 14, 2019**. *Latitudinal patterns of elasmobranch dietary niche breadth* [Oral Presentation - International Conference]. British Ecological Society, Belfast, NI.

{{% font-color magenta %}}**Popov S**{{% /font-color %}}. **Dec 05, 2019**. *Latitudinal patterns of elasmobranch niche breadth* [Oral Presentation - Invited Guest Lecture]. Host: Dr. David Sims, Marine Biological Association, Plymouth, UK.

{{% font-color magenta %}}**Popov S**{{% /font-color %}}, Barkley A, Hussey, NE. **May 13-17, 2019**. *Breadth of the Wild: global patterns of dietary niche across latitude in the marine realm* [Oral Presentation - National Conference]. Canadian Society of Zoology, Windsor, ON, CA.

{{< /pub >}}


### Non-Peer reviewed contributions

{{< pub >}}

{{% font-color magenta %}}**Popov S**{{% /font-color %}} (2021). *Mapping ‘niche’ from the mountaintops to the deep sea.* The Marine Biologist Magazine (19), pp. 14-17. July 2021. *(Cover story. Contributed both article and cover illustration.)*

{{< /pub >}}
