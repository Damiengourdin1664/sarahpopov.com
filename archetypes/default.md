---
title: "{{ replace .Name "-" " " | title }}"
subtitle:
date: {{ .Date }}
draft: true
categories:
tags:
summary:
featured_image:
featured_alt:
featured: false
---

{{< post-image src="" caption="" />}}